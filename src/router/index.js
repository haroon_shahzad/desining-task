import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

// pages
import Compaigns from "../views/Compaigns/Compaigns";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/compaigns",
    name: "Compaigns",
    component: Compaigns,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
